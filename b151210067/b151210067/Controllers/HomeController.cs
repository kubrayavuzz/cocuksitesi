﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b151210067.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
              return View();
        }
        public ActionResult Kızcocuk()
        {
            ViewBag.Message = "Kız çocuk sayfasındasınız.";

            return View();
        }
        public ActionResult Erkekcocuk()
        {
            ViewBag.Message = "Erkek çocuk sayfasındasınız.";

            return View();
        }
        public ActionResult Aksesuar()
        {
            ViewBag.Message = "Aksesuar sayfasındasınız.";

            return View();
        }
        public ActionResult Magazalarımız()
        {
            ViewBag.Message = "Mağazalarımız sayfasındasınız.";

            return View();
        }
    }
}