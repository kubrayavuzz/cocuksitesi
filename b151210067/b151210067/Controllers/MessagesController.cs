﻿using b151210067.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b151210067.Controllers
{
    public class MessagesController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        [HttpPost]
        public ActionResult NewMessage(string Body, string SenderName, string SenderEmail, string SenderUserName, string PhoneNumber)
        {
            if (ModelState.IsValid)
            {
                context.Messages.Add(new Message { Body = Body, SenderEmail = SenderEmail, SenderName = SenderName, PhoneNumber=PhoneNumber, SenderUserName = SenderUserName });
                context.SaveChanges();
            }
            return RedirectToAction("Contact", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (context != null)
            {
                context.Dispose();
            }
        }
    }
}