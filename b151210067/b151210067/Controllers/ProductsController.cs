﻿using b151210067.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b151210067.Controllers
{
    public class ProductsController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        public ActionResult Index(string category)
        {
            var model = context.Products.Where(p => p.Category.Name == category);
            ViewBag.Category = category;
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (context != null)
            {
                context.Dispose();
            }
        }
    }
}