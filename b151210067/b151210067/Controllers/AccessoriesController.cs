﻿using b151210067.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b151210067.Controllers
{
    public class AccessoriesController : Controller
    {
        // GET: Accessory
        ApplicationDbContext context = new ApplicationDbContext();
        public ActionResult Index()
        {
            var model = context.Accessories;
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (context != null)
            {
                context.Dispose();
            }
        }
    }
}