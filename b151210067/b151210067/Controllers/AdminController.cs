﻿using b151210067.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b151210067.Controllers
{
    public class AdminController : Controller
    {

        ApplicationDbContext context = new ApplicationDbContext();


        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NewFurniture()
        {
            return View();
        }



        public ActionResult NewProduct()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewProduct(NewProductViewModel model, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                byte[] content;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    content = binaryReader.ReadBytes(file.ContentLength);
                    context.Products.Add(new Product
                    {
                        Image = content,
                        Name = model.Name,
                        Category = context.Categories.Where(c => c.Name == model.Category).FirstOrDefault(),
                        Price = model.Price,
                        Rating = model.Rating
                    });
                    context.SaveChanges();
                }
            }
            return View();
        }

        public ActionResult NewAccessory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewAccessory(NewAccessoryViewModel model, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                byte[] content;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    content = binaryReader.ReadBytes(file.ContentLength);
                    context.Accessories.Add(new Accessory
                    {
                        Image = content,
                        Name = model.Name,                      
                        Price = model.Price,
                        Rating = model.Rating
                    });
                    context.SaveChanges();
                }
            }
            return View();
        }

        public ActionResult CustomerMessages()
        {

            return View(context.Messages);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (context != null)
            {
                context.Dispose();
            }
        }
    }
}