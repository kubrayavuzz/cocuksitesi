﻿using b151210067.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b151210067.Controllers
{
    public class FileController : Controller
    {
        
        public ActionResult Index([Bind(Prefix = "id")]int furnitureId)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var model = context.Products.Find(furnitureId).Image;
            return File(model, "image/jpeg");
        }

        public ActionResult GetAccessoryImage(int AccessoryId)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var model = context.Accessories.Find(AccessoryId).Image;
            return File(model, "image/jpeg");
        }
    }
}