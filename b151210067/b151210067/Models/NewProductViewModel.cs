﻿

namespace b151210067.Models
{
    public class NewProductViewModel
    {
        public string Category { get; set; }
        public int Price { get; set; }
        public string Name { get; set; }
        public int Rating { get; set; }
    }
}