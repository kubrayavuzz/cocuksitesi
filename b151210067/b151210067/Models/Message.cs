﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b151210067.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string SenderName { get; set; }
        public string SenderUserName { get; set; }
        public string SenderEmail { get; set; }
        public string Body { get; set; }
        public string PhoneNumber { get; set; }
    }
}