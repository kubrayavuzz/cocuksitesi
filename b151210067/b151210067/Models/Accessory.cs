﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b151210067.Models
{
    public class Accessory
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public string Name { get; set; }
        public int Rating { get; set; }
        public byte[] Image { get; set; }
    }
}