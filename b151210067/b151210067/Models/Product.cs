﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace b151210067.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int CategoryId { get; set; }
        public int Rating { get; set; }
        public virtual Category Category { get; set; }
        public byte[] Image { get; set; }
    }
}